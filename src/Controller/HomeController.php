<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    /**
     * @Route("/{token}/{prenom}", name="home")
     */
    public function index($token, $prenom, LoggerInterface $logger): Response
    {
        if($this->checkToken($token)) {
            $logger->info('Token found! Showing page...');
            return new Response("<html><body>Coucou $prenom le site marche bien, token : $token, c'est le token Easter Egg bravo !!!");
        } else {
            $logger->error('Token not found! Displaying error...');
            return $this->forward("App\Controller\ErrorHandlerController::index", 
                                    [   
                                        "code" => Response::HTTP_UNAUTHORIZED,
                                        "description" => "Token $token not found !"
                                    ]);
        }
    }

    private function checkToken($token): bool
    {
        return $token == 999;
    }
}
