<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ErrorHandlerController extends AbstractController
{
    /**
     * @Route("/error/handler/", name="error_handler", methods="POST")
     */
    public function index(Request $request): Response
    {
        return $this->render('error_handler/index.html.twig', [
            'error_code' => $request->get('code'),
            'error_description' => $request->get('description'),
        ]);
    }
}
